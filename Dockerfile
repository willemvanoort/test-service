FROM openjdk:8-jre
MAINTAINER sijmen en willem

COPY target/som-service-1.1-jar-with-dependencies.jar /home/service.jar

EXPOSE 80

CMD ["java", "-jar", "/home/service.jar"]